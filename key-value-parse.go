package main

import (
	"bufio"
	"encoding/hex"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

type Hash [32]byte
type Signature [64]byte

type Cosignature struct {
	keyHash   Hash
	signature Signature
}

type CosignedHead struct {
	timestamp    int64
	treeSize     int64
	rootHash     Hash
	signature    Signature
	cosignatures []Cosignature
}

func parseLine[T interface{}](line string, name string, convert func(string) (T, error)) (T, error) {
	equals := strings.Index(line, "=")
	if equals < 0 {
		var none T
		return none, fmt.Errorf("invalid input line: %q", line)
	}
	key, value := line[:equals], line[equals+1:]
	if key != name {
		var none T
		return none, fmt.Errorf("invalid input line, expected %v, got key: %q", name, key)
	}

	return convert(value)
}

func scanLine[T interface{}](scanner *bufio.Scanner, name string, convert func(string) (T, error)) (T, error) {
	if !scanner.Scan() {
		var none T
		return none, fmt.Errorf("unexpected end of data")
	}
	return parseLine(scanner.Text(), name, convert)
}

func parseInt(s string) (int64, error) {
	// Use ParseUint, to not accept leading +/-.
	i, err := strconv.ParseUint(s, 10, 64)
	if err != nil {
		return 0, err
	}
	if i < 0 || i >= (1<<63) {
		return 0, fmt.Errorf("integer %d (0x%x) is out f range", i, i)
	}
	return int64(i), nil
}

func parseHash(in string) (out Hash, err error) {
	b, err := hex.DecodeString(in)
	if err != nil {
		return
	}
	if len(b) != 32 {
		err = fmt.Errorf("invalid hash length %d", len(b))
		return
	}
	copy(out[:], b)
	return
}

func parseSignature(in string) (out Signature, err error) {
	b, err := hex.DecodeString(in)
	if err != nil {
		return
	}
	if len(b) != 64 {
		err = fmt.Errorf("invalid signature length %d", len(b))
		return
	}
	copy(out[:], b)
	return
}

func parseCosignature(in string) (out Cosignature, err error) {
	comma := strings.Index(in, ",")
	if comma < 0 {
		return Cosignature{}, fmt.Errorf("invalid cosignature line: %q", in)
	}
	hash, err := parseHash(in[:comma])
	if err != nil {
		return Cosignature{}, err
	}
	signature, err := parseSignature(in[comma+1:])
	if err != nil {
		return Cosignature{}, err
	}
	return Cosignature{keyHash: hash, signature: signature}, nil
}

func ParseCosignedHead(input io.Reader) (CosignedHead, error) {
	scanner := bufio.NewScanner(input) // By default, scans by lines

	timestamp, err := scanLine(scanner, "timestamp", parseInt)
	if err != nil {
		return CosignedHead{}, err
	}
	treeSize, err := scanLine(scanner, "tree_size", parseInt)
	if err != nil {
		return CosignedHead{}, err
	}
	rootHash, err := scanLine(scanner, "root_hash", parseHash)
	if err != nil {
		return CosignedHead{}, err
	}
	signature, err := scanLine(scanner, "signature", parseSignature)
	if err != nil {
		return CosignedHead{}, err
	}
	cosignatures := []Cosignature{}
	for scanner.Scan() {
		cosignature, err := parseLine(scanner.Text(), "cosignature", parseCosignature)
		if err != nil {
			return CosignedHead{}, err
		}
		cosignatures = append(cosignatures, cosignature)
	}
	return CosignedHead{timestamp: timestamp, treeSize: treeSize, rootHash: rootHash,
		signature: signature, cosignatures: cosignatures,
	}, nil
}

func main() {
	cosigned, err := ParseCosignedHead(os.Stdin)
	if err != nil {
		fmt.Fprintf(os.Stderr, "parsing failed: %v\n", err)
		os.Exit(1)
	}
	fmt.Printf("got: %#v\n", cosigned)
}
